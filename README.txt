// README.txt,v 0.1 2011/05/02 xalen

To get this module working requires two steps, adding the field to the content
type and outputting the field by modifying the CSS template.

 * Set up the fields you want prefixing them with _css_ so that they read:
   field_css_[your_field_name].
 * Copy template/css.tpl.php into a nodecss folder within your files directory
   and edit as necessary.

You may test the module with the Bartik theme by adding the field
"field_css_wrapper_bg_image". There is no need to copy the CSS template for this
fieldname to work.

Please see the comments in css.tpl.php for more examples and instructions.