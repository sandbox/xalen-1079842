<?php
/**
 * @file
 * CSS template for Node CSS
 *
 * General usage
 * =============
 * Below are two examples for background colors and images.
 *
 * In general you need to find the value you need from the $css_vars array.
 * You can do this using the devel module and dsm($css_vars); or 
 * print_r($css_vars);
 * In the Node CSS settings you can turn on devel which will output the
 * $css_vars array when the node is viewed.
 *
 * Ensure you only write the CSS line if there is a value in the field by
 * checking the field with an if statement, example below.
 *
 * Using an image
 * ==============
 * Get the image style you want from:
 * Configuration > Media > Image styles
 *
 * <?php
 *   if (!empty($css_vars['field_css_wrapper_bg_image']['und'])):
 *   $image = file_load($css_vars['field_css_wrapper_bg_image']['und'][0]['fid']);
 *   $image_url = image_style_url('large', $image->uri);
 * ?>
 *
 * body.page-node-<?php print $css_vars['nid']; ?> #main {
 *   background: url(<?php print $image_url;?>)  no-repeat bottom right;
 * }
 * <?php endif; ?>
 *
 *
 * Using a color
 * =============
 * You could manually enter an HTML color notation in the field but this
 * module has been tested with the jquery_colorpicker module:
 * Replace 'value' with 'jquery_colorpicker' and ensure you have '#' in the
 * right place.
 *
 * <?php if (!empty($css_vars['field_css_wrapper_bg_color']['und'])): ?>
 * body.page-node-<?php print $css_vars['nid']; ?> #page,
 * body.page-node-<?php print $css_vars['nid']; ?> #main-wrapper {
 *   background: #<?php print $css_vars['field_css_wrapper_bg_color']['und'][0]['value']; ?>;
 * }
 * <?php endif; ?>
 */

?>
<?php if (!empty($css_vars['field_css_wrapper_bg_color']['und'])): ?>
body.page-node-<?php print $css_vars['nid']; ?> #page,
body.page-node-<?php print $css_vars['nid']; ?> #main-wrapper {
  background: #<?php print $css_vars['field_css_wrapper_bg_color']['und'][0]['value']; ?>;
}
<?php endif; ?>

<?php
  if (!empty($css_vars['field_css_wrapper_bg_image']['und'])):
  $image = file_load($css_vars['field_css_wrapper_bg_image']['und'][0]['fid']);
  $image_url = image_style_url('large', $image->uri);
?>

body.page-node-<?php print $css_vars['nid']; ?> #main {
  background: url(<?php print $image_url;?>)  no-repeat bottom right;
}
<?php endif; ?>
